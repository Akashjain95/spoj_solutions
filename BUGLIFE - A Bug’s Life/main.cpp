#include <bits/stdc++.h>
using namespace std;
bool bipartiteutil(vector<int> adj[],int i,int color[])
{
queue<int>q;
q.push(i);
color[i]=1;
while(!q.empty())
{
int f=q.front();
q.pop();
for(int i=0;i<adj[f].size();i++)
{
int z=adj[f][i];
if(color[z]==color[f])
return 1;
else if(color[z]==-1)
{
color[z]=1-color[f];
q.push(z);
}
}
}
return 0;
}
bool bipartite(vector<int>adj[],int n)
{
int color[n];
for(int i=0;i<n;i++)
color[i]=-1;
for(int i=0;i<n;i++)
{
if(color[i]==-1)
{
if(bipartiteutil(adj,i,color))
{return 1;}
}
}
return 0;
}
int main()
{
int t;
cin>>t;
int i=1;
while(t--)
{
int n,e;
cin>>n>>e;
vector<int>adj[n];
for(int i=0;i<e;i++)
{
int a,b;
cin>>a>>b;
adj[a-1].push_back(b-1);
adj[b-1].push_back(a-1);
}
cout<<"Scenario #"<<i<<":"<<endl;
i++;
int ans=bipartite(adj,n);
if(ans)
cout<<"Suspicious bugs found!"<<endl;
else
cout<<"No suspicious bugs found!"<<endl;
}
}
