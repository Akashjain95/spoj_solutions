#include <bits/stdc++.h>
using namespace std;
long long dist[1001],n;
bool mstset[1001];
long long graph[1001][1001];

long long mindist(long long src)
{
long long min_val=LONG_MAX;
long long min_index;
for(long long i=0;i<n;i++)
{
if(!mstset[i]&&dist[i]<min_val)
{
min_val=dist[i];
min_index=i;
}
}
return min_index;
}
long long prims(long long src,vector<pair<int,int> >adj[])
{
long long parent[1001];
for(long long i=0;i<1001;i++)
{
dist[i]=LONG_MAX;
mstset[i]=false;
}
dist[0]=0;
parent[0]=-1;
long long ans=0;
for(long long i=0;i<n;i++)
{
long long u=mindist(src);
mstset[u]=true;
//cout<<u<<" "<<dist[u]<<endl;
ans+=dist[u];
for(long long j=0;j<adj[u].size();j++)
{
long long v=adj[u][j].first;
long long w=adj[u][j].second;
if(!mstset[v]&&dist[v]>w)
dist[v]=w;

}
}
return ans;
}
int main()
{
long long t;
cin>>t;
while(t--)
{
long long p,m;
cin>>p>>n>>m;    //n==nodes m==edges
vector<pair<int,int> >adj[1001];
for(long long i=0;i<m;i++)
{
long long a,b,c;
cin>>a>>b>>c;
a--;
b--;
graph[a][b]=c;
graph[b][a]=c;
adj[a].push_back(make_pair(b,c));
adj[b].push_back(make_pair(a,c));
}
cout<<prims(0,adj)*p<<endl;
}
}
