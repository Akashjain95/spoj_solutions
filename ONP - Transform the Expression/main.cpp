#include <bits/stdc++.h>
using namespace std;
//+, -, *, /, ^
bool isoperator(char ch)
{
if(ch=='+'||ch=='-'||ch=='*'||ch=='/'||ch=='^'||ch=='('||ch==')')
return true;
return false;
}
int precedence(char ch)
{
if(ch=='+')
return 1;
if(ch=='-')
return 2;
if(ch=='*')
return 3;
if(ch=='/')
return 4;
if(ch=='^')
return 5;
return -1;
}
int main()
{
int t;
cin>>t;
while(t--)
{
string input;
cin>>input;
stack<char>st;

for(int i=0;i<input.length();i++)
{
if(!isoperator(input[i]))
cout<<input[i];
else if(input[i]=='(')
st.push('(');
else if(input[i]==')')
{
while(st.top()!='(')
{
cout<<st.top();
st.pop();
}
st.pop();
}
else
{
while(!st.empty()&&(precedence(input[i])<=precedence(st.top())))
{
cout<<st.top();
st.pop();
}
st.push(input[i]);
}
}
while(!st.empty())
{
cout<<st.top();
st.pop();
}
cout<<endl;
}
}
