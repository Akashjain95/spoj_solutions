#include <bits/stdc++.h>
using namespace std;
void pv(vector<int>v)
{
for(int i=0;i<v.size();i++)
cout<<v[i];
}
void multiply_vectors(vector<int>&a,vector<int>b)
{
int carry=0;
int value_of_b=0;

for(int i=0;i<b.size();i++)
value_of_b=value_of_b*10+b[i];

vector<int> res;
for(int i=a.size()-1;i>=0;i--)
{
int temp=a[i]*value_of_b+carry;
res.push_back(temp%10);
carry=temp/10;
}
while(carry!=0)
{
res.push_back(carry%10);
carry=carry/10;
}
reverse(res.begin(),res.end());

a.clear();
for(int i=0;i<res.size();i++)
a.push_back(res[i]);
}
vector<int> vectorize(int n)
{
vector<int> vec;
while(n)
{
vec.push_back(n%10);
n=n/10;
}
reverse(vec.begin(),vec.end());
return vec;
}

int main()
{
int t;
cin>>t;
while(t--)
{
int n;
cin>>n;
if(n==0||n==1)
{
cout<<1<<endl;
continue;
}
ofstream fout;
fout.open ("op.txt");
vector<int>res;
res.push_back(1); //initialize result as 1
for(int i=n;i>=1;i--)
{
vector<int> curr=vectorize(i);
multiply_vectors(res,curr);
}
pv(res);
for(int i=0;i<res.size();i++)fout<<res[i];
cout<<endl;
}
}
