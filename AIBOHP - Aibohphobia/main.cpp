#include <bits/stdc++.h>
using namespace std;
int dp[6101][6101];
int main()
{
int t;
cin>>t;
while(t--)
{
string s;
cin>>s;
int n=s.length();
for(int len=1;len<=n;len++)
{
for(int i=0;i<n;i++)
{
int j=i+len-1;
if(i==j)
dp[i][j]=0;
else if(j==i+1)
{
if(s[i]==s[j])
dp[i][j]=0;
else
dp[i][j]=1;
}
else
{
if(s[i]!=s[j])
dp[i][j]=1+min(dp[i][j-1],dp[i+1][j]);
else
dp[i][j]=dp[i+1][j-1];
}
}
}
cout<<dp[0][n-1]<<endl;
}
}
