#include <bits/stdc++.h>
using namespace std;
map<int,long long int>dp;
long long int recur_fun(long long int n)
{
if(n==0)
return 0;

if(dp[n]!=0)
return dp[n];

dp[n]=max(n,recur_fun(n/2)+recur_fun(n/3)+recur_fun(n/4));
return dp[n];
}
int main()
{
long long int n;
while(scanf("%lld",&n)==1)
cout<<recur_fun(n)<<endl;

}
