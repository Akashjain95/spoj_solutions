#include <bits/stdc++.h>
using namespace std;

int main()
{
int c;
cin>>c;
while(c--)
{
int n,k;
cin>>n>>k;

int arr[k+1],dp[k+1];
for(int i=0;i<k;i++)
{
cin>>arr[i];
dp[i]=INT_MAX;
}

dp[k]=INT_MAX;
dp[0]=0;

for(int i=1;i<=k;i++)
{
if(arr[i-1]!=-1)
dp[i]=arr[i-1];

for(int j=1;j<=i/2;j++)
{
if(dp[j]!=INT_MAX&&dp[i-j]!=INT_MAX)
{
dp[i]=min(dp[i],dp[j]+dp[i-j]);
//cout<<i<<" "<<dp[j]<<" "<<dp[i-j]<<endl;
}
}
}
if(dp[k]==INT_MAX)
dp[k]=-1;
cout<<dp[k]<<endl;
}
}
