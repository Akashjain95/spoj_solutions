#include <bits/stdc++.h>
using namespace std;

int main()
{
int t;
cin>>t;
while(t--)
{
int n;
cin>>n;
long long int ans=n;
for(int i=2;i<=sqrt(n);i++)
{
if(n%i==0)
{
ans=ans-ans/i;
while(n%i==0)
n=n/i;
}
}
if(n>1)
ans=ans-(ans/n);
cout<<ans<<endl;
}
}

