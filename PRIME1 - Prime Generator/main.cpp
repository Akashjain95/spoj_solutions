#include <bits/stdc++.h>
using namespace std;
//http://www.geeksforgeeks.org/sieve-of-eratosthenes/
//1. find all primes <= sqrt(10^9) i.e  aprrox 32000
//2. To check x is non prime it should be divisible by any no <=sqrt(x) in other words it has to have one factor <=sqrt(x)
//for eg for 21 sqrt(21)~4 7*3 =21 7>sqrt(21) then 3 <sqrt(21)
//3.mark all number between m and n as non prime using this array in 1.
bool prime[32000];
vector<int>Prime;
void Sieve()
{   long long int n=32000;
    memset(prime, true, sizeof(prime));
    prime[0]=prime[1]=false;
    for (long long int p=2; p*p<=n; p++)
    {// If prime[p] is not changed, then it is a prime
        if (prime[p] == true)
        {
            for (long long int i=p*2; i<=n; i += p)
                prime[i] = false;
        }
    }
for(int i=2;i<=n;i++)
if(prime[i])Prime.push_back(i);
}

int main()
{
Sieve();
int t;
cin>>t;
while(t--)
{
long long int m,n;
cin>>m>>n;
ofstream fout;
fout.open("op.txt");
if(m==1)
m++;
for(;m<=n;m++)
{
int temp=sqrt(m);
int f=1;
for(int i=0;Prime[i]<=temp;i++)
{
if(m%Prime[i]==0)
{
f=0;
break;
}
}
if(f)
{
cout<<m<<endl;
fout<<m<<endl;
}
}
}
}
