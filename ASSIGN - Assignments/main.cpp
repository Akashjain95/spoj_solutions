#include <bits/stdc++.h>
using namespace std;

long long int dp[1<<20+1];
long long int arr[21][21];
long long int n;

long long int fun(long long int s,long long int mask)
{

if(dp[mask]!=-1)
return dp[mask];

if(mask==0)
return 1;

long long int ans=0;
for(long long int i=0;i<n;i++)
{
long long int val=arr[s][n-i-1];
if(val&&(mask&(1<<i)))
{
dp[(mask^(1<<i))]=fun(s+1,mask^(1<<i));
ans=ans+dp[(mask^(1<<i))];
}
}
return ans;
}
/* You are required to complete this method */
#include<bits/stdc++.h>
int minSwaps(int A[], int N)
{
vector<pair<int,int> > vec;

for(int i=0;i<N;i++)
vec.push_back(make_pair(A[i],i));

sort(vec.begin(),vec.end());
int ans=0;
int vis[N+1];

for(int i=0;i<N;i++)
{
if(vis[i]||i==vec[i].second)
;
else
{
int j=i;
int c=0;
while(!vis[j]||vec[j].second!=j)
{
vis[j]=1;
j=vec[j].second;
c++;
}
ans=ans+c-1;
}
}
return ans;
}

int main()
{
int t;
cin>>t;

while(t--)
{
cin>>n;

for(long long int i=0;i<(1<<20+1);i++)
dp[i]=-1;
for(long long int i=0;i<n;i++)
{
for(long long int j=0;j<n;j++)
cin>>arr[i][j];
}
long long int mask=(1<<n)-1;
cout<<fun(0,mask)<<endl;
}
}
