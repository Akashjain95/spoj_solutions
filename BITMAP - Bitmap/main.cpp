#include <bits/stdc++.h>
using namespace std;
char p[183][183];
int n,m;

int row[]={-1,1,0,0};
int col[]={0,0,1,-1};

int bfs(int i,int j,int dist[183][183])
{
int visited[183][183];
for(int i=0;i<183;i++)
for(int j=0;j<183;j++)
visited[i][j]=0;
queue<pair<int,int> >q;

q.push(make_pair(i,j));

dist[i][j]=0;
visited[i][j]=1;

while(!q.empty())
{
pair<int,int>f=q.front();
q.pop();

int i=f.first;
int j=f.second;

for(int c=0;c<4;c++)
{
int temp_i,temp_j;
temp_i=i+row[c];
temp_j=j+col[c];
if(!(temp_i>=0&&temp_j>=0&&temp_i<n&&temp_j<m))
continue;
if(visited[temp_i][temp_j]==0&&dist[temp_i][temp_j]>1+dist[i][j])
{
//cout<<temp_i<<" "<<temp_j<<" ans"<<endl;
visited[temp_i][temp_j]=1;
dist[temp_i][temp_j]=min(dist[temp_i][temp_j],1+dist[i][j]);
q.push(make_pair(temp_i,temp_j));
}
}

}
}
int main()
{
int t;
cin>>t;
while(t--)
{
cin>>n>>m;
int dist[183][183];

for(int i=0;i<183;i++)
for(int j=0;j<183;j++)
dist[i][j]=INT_MAX;

for(int i=0;i<n;i++)
cin>>p[i];

for(int i=0;i<n;i++)
{
for(int j=0;j<m;j++)
{
if(p[i][j]=='1')
bfs(i,j,dist);
}
}
for(int i=0;i<n;i++)
{
for(int j=0;j<m;j++)
cout<<dist[i][j]<<" ";
cout<<endl;
}
}
}
