#include <bits/stdc++.h>
using namespace std;

int dist[8][8];
int row[]={2,2,1,1,-1,-1,-2,-2};
int col[]={-1,1,-2,2,-2,2,-1,1};
void bfs(int x,int y,int dx,int dy)
{
for(int i=0;i<8;i++)
for(int j=0;j<8;j++)
dist[i][j]=INT_MAX;
queue<pair<int,int> >q;

q.push(make_pair(x,y));
dist[x][y]=0;


while(!q.empty())
{
pair<int,int>front=q.front();
q.pop();
int a=front.first;
int b=front.second;

if(a==dx&&b==dy)
{
cout<<dist[a][b]<<endl;
break;
}
for(int i=0;i<8;i++)
{
if(a+row[i]>=0&&a+row[i]<8&&b+col[i]>=0&&b+col[i]<8&&dist[a+row[i]][b+col[i]]==INT_MAX)
{
dist[a+row[i]][b+col[i]]=1+dist[a][b];
q.push(make_pair(a+row[i],b+col[i]));
}
}
}

}
int main()
{
int t;
cin>>t;
while(t--)
{
string s,e;
cin>>s>>e;
int sr,sc,er,ec;
sr=s[1]-'0';
sc=s[0]-'a';
er=e[1]-'0';
ec=e[0]-'a';
sr--;
er--;
//cout<<sr<<" "<<sc<<" "<<er<<" "<<ec<<endl;
bfs(sr,sc,er,ec);
//cout<<"here"<<endl;
}
}
