#include <bits/stdc++.h>
using namespace std;
int mindist(bool sptset[],int dist[],int V)
{
int min_val=INT_MAX,min_index;
for(int i=0;i<V;i++)
{
if(!sptset[i]&&dist[i]<min_val)
{
min_val=dist[i];
min_index=i;
}
}
return min_index;
}
void dijikstra(vector<pair<int,int> > adj[],int V,int t)
{
bool sptset[V+1];
int dist[V+1];
for(int i=0;i<V;i++)
{
sptset[i]=false;
dist[i]=INT_MAX;
}
dist[0]=0;
for(int count=0;count<V-1;count++)
{
int u=mindist(sptset,dist,V);
sptset[u]=true;
/*for(int i=0;i<V;i++)
{
if(g[u][i]&&!sptset[i]&&dist[i]>dist[u]+g[u][i])
dist[i]=dist[u]+g[u][i];
}*/
for(int i=0;i<adj[u].size();i++)
{
int v=adj[u][i].first;
if(!sptset[v]&&dist[v]>dist[u]+adj[u][i].second)
dist[v]=dist[u]+adj[u][i].second;
}
}
int ans=0;
for(int i=0;i<V;i++)
{
if(dist[i]<=t)
ans++;
}
cout<<ans<<endl;
}
int main()
{
int n,e,t,m;
cin>>n>>e>>t>>m;
vector<pair<int,int> > adj[n+1];
for(int i=0;i<m;i++)
{
int a,b,c;
cin>>a>>b>>c;
a--;
b--;
adj[b].push_back(make_pair(a,c));
}
dijikstra(adj,n,t);
}
