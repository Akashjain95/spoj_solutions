#include <bits/stdc++.h>
using namespace std;
char arr[102][102];
int row[]={-1,1,-1,1,0,0,-1,1};
int col[]={1,-1,-1,1,-1,1,0,0};
int r,c;
int visited[101][101];
string s="ALLIZZWELL";

int dfs(int i,int j,int pos)
{
visited[i][j]=1;
if(pos==s.length())
return 1;
for(int k=0;k<8;k++)
{
if(i+row[k]>=0&&j+col[k]>=0&&i+row[k]<r&&j+col[k]<c&&arr[i+row[k]][j+col[k]]==s[pos]&&!visited[i+row[k]][j+col[k]])
{
if(dfs(i+row[k],j+col[k],pos+1))
return 1;
}
}
visited[i][j]=0;
return 0;
}
int main()
{
int t;
cin>>t;
while(t--)
{
cin>>r>>c;
for(int i=0;i<r;i++)
cin>>arr[i];

int f=0;
for(int i=0;i<r;i++)
{
for(int j=0;j<c;j++)
{
if(arr[i][j]=='A')
{
for(int x=0;x<101;x++)
for(int y=0;y<101;y++)
visited[x][y]=0;
visited[i][j]=1;
if(dfs(i,j,1))
{
f=1;
break;
}
}
}
}
if(f==1)
cout<<"YES"<<endl;
else
cout<<"NO"<<endl;
}
}
