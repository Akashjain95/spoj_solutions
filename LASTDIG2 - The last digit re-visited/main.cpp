#include <bits/stdc++.h>
using namespace std;

int len[]={1,1,4,4,2,1,1,4,4,2};
vector<vector<int> >cycles;

void pre()
{
vector<int>temp;
for(int i=0;i<10;i++)
{
temp.clear();
int k=i;
for(int j=0;j<len[i];j++)
{
temp.push_back(k);
k=(k*i)%10;
}
cycles.push_back(temp);
}
}
int main()
{
pre();
unsigned long long int b;
string a;
int t;
cin>>t;
while(t--)
{
cin>>a>>b;
int A=a[a.length()-1]-'0';
//cout<<A<<endl;
if(b==0)
{
cout<<1<<endl;
continue;
}
int pos=(b-1)%len[A];
cout<<cycles[A][pos]<<endl;
}
}
