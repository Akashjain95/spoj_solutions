#include <iostream>

using namespace std;

int main()
{
long long t;
cin>>t;
long long x=0;
while(t--)
{
long long n;
cin>>n;
x++;
long long arr[n+1];
long long dp[n+1];
for(long long i=0;i<=n;i++)
dp[i]=0;
for(long long i=0;i<n;i++)
cin>>arr[i];
for(long long i=1;i<=n;i++)
{
if(i==1)
dp[i]=arr[i-1];
else
dp[i]=max(dp[i-1],arr[i-1]+dp[i-2]);
}
cout<<"Case "<<x<<": "<<dp[n]<<endl;
}
}
