#include <bits/stdc++.h>
using namespace std;
int dist[501];
int graph[501][501];

int mindist(bool sptset[],int dist[])
{
int min_val=INT_MAX;
int min_index;
for(int i=0;i<501;i++)
{
if(!sptset[i]&&dist[i]<min_val)
{
min_val=dist[i];
min_index=i;
}
}
return min_index;
}

void dijikstra(int src)
{
bool sptset[501];

for(int i=0;i<501;i++)
{
sptset[i]=false;
dist[i]=INT_MAX;
}
dist[src]=0;

for(int i=0;i<500;i++)
{
int u=mindist(sptset,dist);
sptset[u]=true;

for(int j=0;j<501;j++)
{
if(graph[u][j]!=INT_MAX&&!sptset[j]&&dist[u]!=INT_MAX&&dist[u]+graph[u][j]<dist[j])
dist[j]=dist[u]+graph[u][j];
}
}
}
int main()
{
int n;
cin>>n;
map<int,int>node;

for(int i=0;i<501;i++)
{
for(int j=0;j<501;j++)
graph[i][j]=INT_MAX;
}

for(int i=0;i<n;i++)
{
int a,b,w;
cin>>a>>b>>w;
node[a]=1;
node[b]=1;
graph[a][b]=min(graph[a][b],w);
graph[b][a]=min(graph[b][a],w);
}
int u,q,v;
cin>>u>>q;
int f=1;
if(node[u]==0)
f=0;
else
dijikstra(u);

for(int i=0;i<q;i++)
{
cin>>v;
if(f==0||node[v]==0)
cout<<"NO PATH"<<endl;
else
{
if(dist[v]!=INT_MAX)
cout<<dist[v]<<endl;
else
cout<<"NO PATH"<<endl;
}
}
}
