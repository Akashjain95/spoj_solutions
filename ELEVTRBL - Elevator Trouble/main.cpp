#include <bits/stdc++.h>
using namespace std;
void bfs(int s,int g,int f,int u,int d)
{
int dist[f+1];
for(int i=0;i<f+1;i++)
dist[i]=INT_MAX;

queue<int>q;
dist[s]=0;
q.push(s);

int flag=0;

while(!q.empty())
{
int front=q.front();
q.pop();
//cout<<"f="<<front<<endl;

if(front==g)
{
flag=1;
cout<<dist[front]<<endl;
}
if(front+u<=f&&dist[front+u]==INT_MAX)
{
dist[front+u]=dist[front]+1;
q.push(front+u);
}
if(front-d>=1&&dist[front-d]==INT_MAX)
{
dist[front-d]=dist[front]+1;
q.push(front-d);
}
}

if(!flag)
cout<<"use the stairs"<<endl;
}
int main()
{
int f,s,g,u,d;
cin>>f>>s>>g>>u>>d;

if(s==g)
cout<<0<<endl;
else
bfs(s,g,f,u,d);
}
