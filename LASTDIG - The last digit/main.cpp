#include <bits/stdc++.h>
using namespace std;
int cycle_len[]={4,4,2,1,1,4,4,2};  // for 2,3,4,5,6,7,8,9
vector<vector<int> > cycles;

int main()
{
//generate all cycles.....
for(int i=2;i<=9;i++)
{
int a=i;
vector<int>cycle;
for(int j=0;j<cycle_len[i-2];j++)
{
cycle.push_back(a%10);
a=a*i;
}
cycles.push_back(cycle);
cycle.clear();
}

int t;
cin>>t;

while(t--)
{
int a,b;
cin>>a>>b;
a=a%10;
if(a==1||b==0)
cout<<1<<endl;
else if(a==0)
cout<<0<<endl;
else
{
b=(b-1)%cycle_len[a-2];
//cout<<"b="<<b<<endl;
cout<<cycles[a-2][b]<<endl;
}
}
}
