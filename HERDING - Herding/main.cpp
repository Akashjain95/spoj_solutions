#include <bits/stdc++.h>
using namespace std;
char graph[1001][1001];
int visited[1001][1001];
int n,m;
int dfsutil(int i,int j,int call)
{
//cout<<i<<" "<<j<<endl;
visited[i][j]=call;
if(graph[i][j]=='S'&&i<n-1)
{
if(!visited[i+1][j])
return dfsutil(i+1,j,call);
else if(visited[i+1][j]<call)
return 0;
}
else if(graph[i][j]=='N'&&i>0)
{
if(!visited[i-1][j])
return dfsutil(i-1,j,call);
else if(visited[i-1][j]<call)
return 0;
}

else if(graph[i][j]=='E'&j<m-1)
{
if(!visited[i][j+1])
return dfsutil(i,j+1,call);
else if(visited[i][j+1]<call)
return 0;
}
else if(graph[i][j]=='W'&&j>0)
{
if(!visited[i][j-1])
return dfsutil(i,j-1,call);
else if(visited[i][j-1]<call)
return 0;
}
return 1;
}
int dfs(int x,int y)
{
int call=0;
for(int i=0;i<n;i++)
{
for(int j=0;j<m;j++)
visited[i][j]=0;
}
int ans=0;
for(int i=0;i<n;i++)
{
for(int j=0;j<m;j++)
{
if(!visited[i][j])
{
call++;
//cout<<i<<" "<<j<<" ";
int temp=dfsutil(i,j,call);
//cout<<temp<<endl;
ans+=temp;
}
}
}
cout<<ans<<endl;
}
int main()
{
cin>>n>>m;
for(int i=0;i<n;i++)
cin>>graph[i];
dfs(0,0);
}
