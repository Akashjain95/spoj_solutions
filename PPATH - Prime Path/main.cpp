//http://www.geeksforgeeks.org/sieve-of-eratosthenes/
#include <bits/stdc++.h>
using namespace std;

vector<int> prime_list;
vector<int>adj[9999];
int dist[9999];
void SieveOfEratosthenes(int n)
{
    bool prime[n+1];
    memset(prime, true, sizeof(prime));

    for (int p=2; p*p<=n; p++)
    {
        // If prime[p] is not changed, then it is a prime
        if (prime[p] == true)
        {
            // Update all multiples of p
            for (int i=p*2; i<=n; i += p)
                prime[i] = false;
        }
    }
    for (int p=2; p<=n; p++)
       if (prime[p]&&p>=1000)
        prime_list.push_back(p);
}

int differ(int a,int b)
{
int d=0;
for(int i=0;i<4;i++)
{
if(a%10!=b%10)
d++;
a=a/10;
b=b/10;
}
if(d==1)
return 1;
return 0;
}

void bfs(int src,int dest)
{
queue<int>q;
q.push(src);
dist[src]=0;

while(!q.empty())
{
int front=q.front();
q.pop();
if(front==dest)
{
cout<<dist[front]<<endl;
break;
}
for(int i=0;i<adj[front].size();i++)
{
if(dist[adj[front][i]]==INT_MAX)
{
dist[adj[front][i]]=1+dist[front];
q.push(adj[front][i]);
}
}
}

}
int main()
{
    int n = 10000;
    SieveOfEratosthenes(n);
    for(int i=0;i<prime_list.size();i++)
    {
     for(int j=i+1;j<prime_list.size();j++)
     {
      if(differ(prime_list[i],prime_list[j]))
         {
            adj[prime_list[i]].push_back(prime_list[j]);
            adj[prime_list[j]].push_back(prime_list[i]);
         }
     }
    }
int t;
cin>>t;
while(t--)
{
int a,b;
cin>>a>>b;
for(int i=0;i<9999;i++)
dist[i]=INT_MAX;
bfs(a,b);
}
    return 0;
}
